from jvm import jvm
from command import updateJVM
from mem import mem
from command import asm
from cache import cache
from math import log

test_cases = [
    ('''    nop
            bitpush 3
            bitpush 5
            iadd
            bitpush 3
            isub
            bitpush 9
            istore 2
            iinc 2 3
            iload 2
            ''',
     [0, 3, 5, 8, 3, 5, 9, 5, 5, 12]),
    ('''s: iinc 0 1
        iload 0
        goto s
    ''',
     [0, 1, 1, 1, 2, 2, 2, 3, 3, 3]),
    ('''
        bitpush 0
        ifeq n1
        bitpush 2
    
n1:     bitpush 1
        ifeq n2
        bitpush 2
    
n2:     bitpush -1
        ifeq n3
        bitpush 3
    
    
n3:     bitpush 0
    ''',
    [0, 0, 1, 0, 2, -1, 2, 3, 0]),

    (
    ''' bitpush 0
        iflt n1
        bitpush 3
    
n1:        bitpush 1
        iflt n2
        bitpush 2
    
n2:        bitpush -1
        iflt n3
        bitpush 12
    
n3:        bitpush 32
    ''',
    [0, 0, 3, 1, 3, 2, -1, 2, 32]),

    (
    ''' bitpush 0
        bitpush 0
        if_icmpeq n1
        bitpush 3
        
    n1: bitpush 1
        bitpush 2
        if_icmpeq n2
        bitpush 3
        
    n2: bitpush -1
        bitpush -1
        if_icmpeq n3
        bitpush 1
    n3: bitpush 2
        ''',
    [0, 0, 0, 1, 2, 0, 3, -1, -1, 3, 2])
]


def test(tos, prog, asm_prog):
    # print(tos)
    # print(prog)
    # print(asm_prog)
    j = jvm()
    j.reset()

    m = mem(128, 0.1, prog)
    c = cache(m, 32, 8, 'LRU', 'indirect')
    pc_hit = [0] * 128

    # m = tmp
    ac = []
    pc = []
    e = updateJVM(j, c)
    e.trace = False
    working = True
    total_delay = 0
    for i in tos:
        k = 0
        x = 0
        while ( x < e.jvm.pc ):
            x += len(asm_prog[k])
            if asm_prog[k][0] == 'goto' or asm_prog[k][0] == 'ifeq' or asm_prog[k][0] == 'iflt' or asm_prog[k][0] == 'if_icmpeq':
                x += 1
            k += 1
        print("\n\n~~~~~~~~~~~~~~~~~~~~~~~~~~~~ Executing " + str(asm_prog[k]) + "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~")


        e.updateJVM()
        pc_hit[e.jvm.pc] += 1
        ac.append(e.mem.fast_read4(e.jvm.sp))
        pc.append(e.jvm.pc)
        if e.mem.fast_read4(e.jvm.sp) != i:
            working = False
            print("hi")

        print("\nCommand delay = " + str(sum(e.delays) + 1) + "\n-------------------------------------------------------------\n")
        total_delay += sum(e.delays) + 1

    k = 0
    x = 0
    util = 0
    total_hit = sum(pc_hit)
    while (x < 128):
        if pc_hit[x] != 0:
            p = pc_hit[x] / total_hit
            util += -p * log(p)
        x += 1
    print(util)
    print("========================\nTotal delay = " + str(total_delay) +
          "\nUtilization = " + str(util) +
          "\nHit ratio = " + str(c.hitCount / c.missCount) +
          "\nThroughput = " + str(len(asm_prog) / total_delay) +
          "\n========================")
    return working, ac, pc

def run_test():

    working = True
    counter = 0
    for i, v in test_cases:
        counter += 1
        print("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@")
        print("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@")
        print("running test ", counter)
        bin, prog = asm(i)
        x, trace, pc = test(v, bin, prog)
        if not x:
            print("failed @ test ", counter)
            print("pc:", pc)
            print("t", trace)
            print("a", v)
            for i in range(len(v)):
                if v[i] != trace[i]:
                    print("stack diverge at command ", i + 1)
                    print(prog[i])
                    break

            working = False
            break

        print('passed test \n========================\n\n', counter)

    return working

if __name__ == '__main__':
    print('running tests')
    if run_test():
        print('passed all tests')