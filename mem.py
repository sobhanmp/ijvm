import random
import math
from bitstring import Bits


class mem:

    def __init__(self, size, delay_ratio, d):
        self.size = size
        self.data = [0] * size
        if size < len(d):
            raise NameError("can't assign memory")
        for i in range(len(d)):
            self.data[i] = d[i]
        self.delayRatio = delay_ratio

    def read1(self, address):
        return [math.floor(random.random() * 4 + 1) * self.delayRatio, self.data[address]]

    def fast_read4(self, address):
        return Bits(bin=
             Bits(uint=self.data[address], length=8).bin +
             Bits(uint=self.data[address + 1], length=8).bin +
             Bits(uint=self.data[address + 2], length=8).bin +
             Bits(uint=self.data[address + 3], length=8).bin
             ).int


    def read4(self, address):
        return [math.floor(random.random() * 4 + 1) * self.delayRatio,
                self.fast_read4(address)]


    def write(self, address, value):
        x = Bits(int=value, length=32).bin
        for i in range(0, 4):
            self.data[int(address + i)] = Bits(bin=x[i * 8: i * 8 + 8]).uint
        return math.floor(random.random() * 4 + 1) * self.delayRatio

# memory = mem()
# memory.write( 0, 120841)
# memory.write( 4, 21344)
# memory.write( 8, 2417950702)
# print(memory.read1(8))
# print(Bits(int = memory.read1(12)[1], length = 8).bin)
